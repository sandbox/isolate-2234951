<?php

/**
 * @file
 * CTools content type plugin to embed an organizational unit info.
 */

$plugin = array(
  'title' => t('Organizational unit info'),
  'no title override' => TRUE,
  'description' => t('Organizational unit info.'),
  'single' => TRUE,
  'defaults' => array(),
  'category' => array(t('Miscellaneous'), -10),
  'edit form' => 'organizational_unit_info_content_type_edit_form',
  'render callback' => 'organizational_unit_info_content_type_render',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Render the content type.
 */
function organizational_unit_info_content_type_render($subtype, $conf, $args, $context) {
  // Return the output as a block.
  $block = new stdClass();
  $block->title = '';
  $block->block = '';

  $node = $context->data;
  $template = 'paddle_organizational_unit_page_info';

  $title = $node->title;
  $head_of_unit = !empty($node->field_paddle_ou_head_unit['und'][0]['value']) ? check_plain($node->field_paddle_ou_head_unit['und'][0]['value']) : '';
  $parent_1 = !empty($node->field_paddle_ou_parent_1['und'][0]['value']) ? check_plain($node->field_paddle_ou_parent_1['und'][0]['value']) : '';
  $parent_2 = !empty($node->field_paddle_ou_parent_2['und'][0]['value']) ? check_plain($node->field_paddle_ou_parent_2['und'][0]['value']) : '';

  $variables = array(
    'title' => check_plain($title),
    'head_of_unit' => $head_of_unit,
    'parent_1' => $parent_1,
    'parent_2' => $parent_2,
  );

  // Allow other modules to alter the fields.
  drupal_alter('paddle_organizational_unit_page_info', $variables);

  $block->content = theme($template, $variables);

  return $block;
}

/**
 * Edit form.
 */
function organizational_unit_info_content_type_edit_form($form, &$form_state) {
  return $form;
}
