<?php

/**
 * @file
 * CTools content type plugin to embed an organizational unit contact
 * information.
 */

$plugin = array(
  'title' => t('Organizational unit contact information'),
  'no title override' => TRUE,
  'description' => t('Organizational unit contact information.'),
  'single' => TRUE,
  'defaults' => array(),
  'category' => array(t('Miscellaneous'), -10),
  'edit form' => 'contact_information_content_type_edit_form',
  'render callback' => 'contact_information_content_type_render',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Render the content type.
 */
function contact_information_content_type_render($subtype, $conf, $args, $context) {
  // Return the output as a block.
  $block = new stdClass();
  $block->title = '';
  $block->block = '';

  $node = $context->data;
  $template = 'paddle_organizational_unit_page_contact_information';

  $website = !empty($node->field_paddle_ou_website['und'][0]['value']) ? check_plain($node->field_paddle_ou_website['und'][0]['value']) : '';
  $website_simple = preg_replace('#^https?://#', '', $website);
  $email = !empty($node->field_paddle_ou_email['und'][0]['email']) ? check_plain($node->field_paddle_ou_email['und'][0]['email']) : '';
  $phone = !empty($node->field_paddle_ou_phone['und'][0]['value']) ? check_plain($node->field_paddle_ou_phone['und'][0]['value']) : '';
  $fax = !empty($node->field_paddle_ou_fax['und'][0]['value']) ? check_plain($node->field_paddle_ou_fax['und'][0]['value']) : '';

  $address = !empty($node->field_paddle_ou_address['und'][0]) ? $node->field_paddle_ou_address['und'][0] : array();
  $empty_address = TRUE;
  foreach ($address as $key => $value) {
    $address[$key] = check_plain($value);
    if ($key != 'country') {
      // Ignore country because that always has a value.
      // Check if any other key has a non empty value.
      if (!empty($address[$key])) {
        $empty_address = FALSE;
      }
    }
  }
  $formatted_address = '';
  if (!$empty_address) {
    $addresses = field_get_items('node', $node, 'field_paddle_ou_address');
    $formatted_address = field_view_value('node', $node, 'field_paddle_ou_address', $addresses[0]);
    $formatted_address = render($formatted_address);
  }

  $variables = array(
    'name' => check_plain($node->title),
    'email' => $email,
    'phone' => $phone,
    'fax' => $fax,
    'website_simple' => $website_simple,
    'website' => $website,
    'parents' => array(
      !empty($node->field_paddle_ou_parent_1['und'][0]['safe_value']) ? $node->field_paddle_ou_parent_1['und'][0]['safe_value'] : '',
      !empty($node->field_paddle_ou_parent_2['und'][0]['safe_value']) ? $node->field_paddle_ou_parent_2['und'][0]['safe_value'] : '',
    ),
    'address' => $address,
    'address_formatted' => $formatted_address,
  );

  $block->content = theme($template, $variables);

  return $block;
}

/**
 * Edit form.
 */
function contact_information_content_type_edit_form($form, &$form_state) {
  return $form;
}
