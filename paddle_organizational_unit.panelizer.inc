<?php
/**
 * @file
 * paddle_organizational_unit.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function paddle_organizational_unit_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:organizational_unit:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'organizational_unit';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'paddle_content_region';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'paddle_2_col_9_3_d';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'nested_top' => NULL,
      'nested_left' => NULL,
      'nested_right' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '238ed114-c194-11e3-b4b6-b6ee55aeb395';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-3f99812e-c194-11e3-b4b6-b6ee55aeb395';
    $pane->panel = 'bottom';
    $pane->type = 'content_region';
    $pane->subtype = 'inherit';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'region' => 'bottom',
      'type' => 'organizational_unit',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array(
      'type' => 'immovable',
      'regions' => array(
        'bottom' => 'bottom',
      ),
    );
    $pane->uuid = '3f99812e-c194-11e3-b4b6-b6ee55aeb395';
    $display->content['new-3f99812e-c194-11e3-b4b6-b6ee55aeb395'] = $pane;
    $display->panels['bottom'][0] = 'new-3f99812e-c194-11e3-b4b6-b6ee55aeb395';
    $pane = new stdClass();
    $pane->pid = 'new-3494ba3a-7a8b-4928-a23a-1cedb3d61498';
    $pane->panel = 'nested_left';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array(
      'type' => 'immovable',
      'regions' => array(
        'bottom' => 'nested_left',
      ),
    );
    $pane->uuid = '3494ba3a-7a8b-4928-a23a-1cedb3d61498';
    $display->content['new-3494ba3a-7a8b-4928-a23a-1cedb3d61498'] = $pane;
    $display->panels['nested_left'][0] = 'new-3494ba3a-7a8b-4928-a23a-1cedb3d61498';
    $pane = new stdClass();
    $pane->pid = 'new-62279c94-115f-42ba-9c6e-c2af9ba18a09';
    $pane->panel = 'nested_right';
    $pane->type = 'contact_information';
    $pane->subtype = 'contact_information';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array(
      'type' => 'immovable',
      'regions' => array(
        'bottom' => 'nested_right',
      ),
    );
    $pane->uuid = '62279c94-115f-42ba-9c6e-c2af9ba18a09';
    $display->content['new-62279c94-115f-42ba-9c6e-c2af9ba18a09'] = $pane;
    $display->panels['nested_right'][0] = 'new-62279c94-115f-42ba-9c6e-c2af9ba18a09';
    $pane = new stdClass();
    $pane->pid = 'new-f05c243e-a358-407a-8e00-816a025594bf';
    $pane->panel = 'nested_top';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_paddle_ou_logo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'paddle_organizational_unit_logo',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f05c243e-a358-407a-8e00-816a025594bf';
    $display->content['new-f05c243e-a358-407a-8e00-816a025594bf'] = $pane;
    $display->panels['nested_top'][0] = 'new-f05c243e-a358-407a-8e00-816a025594bf';
    $pane = new stdClass();
    $pane->pid = 'new-ca53446f-a6f4-4a0e-b469-262c215b873b';
    $pane->panel = 'nested_top';
    $pane->type = 'organizational_unit_info';
    $pane->subtype = 'organizational_unit_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array(
      'type' => 'immovable',
      'regions' => array(
        'bottom' => 'nested_top',
      ),
    );
    $pane->uuid = 'ca53446f-a6f4-4a0e-b469-262c215b873b';
    $display->content['new-ca53446f-a6f4-4a0e-b469-262c215b873b'] = $pane;
    $display->panels['nested_top'][1] = 'new-ca53446f-a6f4-4a0e-b469-262c215b873b';
    $pane = new stdClass();
    $pane->pid = 'new-7c6df9e0-c194-11e3-b4b6-b6ee55aeb395';
    $pane->panel = 'right';
    $pane->type = 'content_region';
    $pane->subtype = 'inherit';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'region' => 'right',
      'type' => 'organizational_unit',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array(
      'type' => 'immovable',
      'regions' => array(
        'right' => 'right',
      ),
    );
    $pane->uuid = '7c6df9e0-c194-11e3-b4b6-b6ee55aeb395';
    $display->content['new-7c6df9e0-c194-11e3-b4b6-b6ee55aeb395'] = $pane;
    $display->panels['right'][0] = 'new-7c6df9e0-c194-11e3-b4b6-b6ee55aeb395';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:organizational_unit:default'] = $panelizer;

  return $export;
}
