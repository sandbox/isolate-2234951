<?php

/**
 * @file
 * Template for the Page view of the Organizational Unit.
 */
?>
<h2 class="paddle-oup-page-title"><?php print $title; ?></h2>
<?php if (!empty($head_of_unit)) : ?>
  <div class="paddle-oup paddle-oup-head-unit">
    <span class="label"><?php print t('Managed by'); ?></span>
    <?php print $head_of_unit; ?>
  </div>
<?php endif;
if (!empty($parent_1)) : ?>
  <div class="paddle-oup paddle-oup-parent-units parent-1">
    <?php print $parent_1; ?>
  </div>
<?php endif;
if (!empty($parent_2)) : ?>
  <div class="paddle-oup paddle-oup-parent-units parent-2">
    <?php print $parent_2; ?>
  </div>
<?php endif; ?>
