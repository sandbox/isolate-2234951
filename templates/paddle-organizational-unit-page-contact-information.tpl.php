<?php

/**
 * @file
 * Template for the Page view of the Organizational Unit.
 */
?>
<?php if (!empty($address_formatted)) : ?>
  <h3 class="paddle-oup-heading"><?php print t('Location'); ?></h3>
  <div class="paddle-oup paddle-oup-address">
    <i class="fa fa-home valigntop"></i>
    <div class="inline-block"><?php print $address_formatted; ?></div>
  </div>
<?php endif;
if (!empty($phone) || !empty($fax) || !empty($email) || !empty($website)) : ?>
  <h3 class="paddle-oup-heading"><?php print t('Contact information'); ?></h3>
  <?php if (!empty($phone)) : ?>
    <div class="paddle-oup paddle-oup-phone">
      <i class="fa fa-phone"></i>
      <div class="inline-block"><?php print $phone; ?></div>
    </div>
  <?php endif;
  if (!empty($fax)) : ?>
    <div class="paddle-oup paddle-oup-fax">
      <i class="fa fa-print"></i>
      <div class="inline-block"><?php print $fax; ?></div>
    </div>
  <?php endif;
  if (!empty($email)) : ?>
    <div class="paddle-oup paddle-oup-email">
      <i class="fa fa-envelope valigntop"></i>
      <a href="mailto:<?php print $email; ?>"><?php print $email; ?></a>
    </div>
  <?php endif;
  if (!empty($website)) : ?>
    <div class="paddle-oup paddle-oup-website">
      <i class="fa fa-link valigntop"></i>
      <a href="<?php print $website; ?>"><?php print $website_simple; ?></a>
    </div>
  <?php endif;
endif;
?>
