<?php

/**
 * @file
 * Install, uninstall, update and schema hooks for the Organizational Unit app.
 */

/**
 * Implements hook_enable().
 */
function paddle_organizational_unit_enable() {
  // We need to load all files from the dependent modules so hook
  // implementations can be found, used by field_create_field() invoked
  // indirectly by this module. Otherwise the addressfield field type and other
  // new field types are not yet known.
  module_load_all();
  drupal_static_reset('module_implements');
  cache_clear_all('field_info_types:', 'cache_field', TRUE);
  module_implements('', FALSE, TRUE);

  features_include_defaults(NULL, TRUE);
  features_revert_module('paddle_organizational_unit');

  // We need to clear the node types cache which is used by
  // user_role_grant_permissions(). Otherwise our new content type is not yet
  // known.
  node_type_cache_reset();

  // We need to clear the CTools plugin cache so our new content type plugin
  // gets picked up before we try to assign the permissions related to that
  // plugin.
  ctools_modules_enabled(array('paddle_organizational_unit'));

  $permissions = array(
    'create organizational_unit content',
    'edit any organizational_unit content',
    'delete any organizational_unit content',
    'edit organizational_unit content in landing pages',
  );

  foreach (array('Chief Editor', 'Editor') as $role_name) {
    $role = user_role_load_by_name($role_name);
    user_role_grant_permissions($role->rid, $permissions);
  }
}

/**
 * Implements hook_update_dependencies().
 */
function paddle_organizational_unit_update_dependencies() {
  $dependencies = array();

  // Migrate from image field to scald atom field before parsing all the
  // nodes to update references.
  $dependencies['paddle'][7108] = array(
    'paddle_organizational_unit' => 7002,
  );

  return $dependencies;
}

/**
 * Give permissions to the organizational unit pane.
 */
function paddle_organizational_unit_update_7000() {
  foreach (array('Chief Editor', 'Editor') as $name) {
    $role = user_role_load_by_name($name);
    user_role_grant_permissions($role->rid, array('edit organizational_unit content in landing pages'));
  }
}

/**
 * Widen the 'field_computed_ou' field so it can take long organizational units.
 */
function paddle_organizational_unit_update_7001() {
  // The maximum length of the field is 3 titles of max 255 characters + 6
  // characters for the ' > ' separators.
  $length = 3 * 255 + 6;

  // The Field module does not allow us to change the database schema of fields
  // that already contain data, but widening the field does not affect the data
  // so let's do it quietly on the tables itself.
  $query = 'SELECT data FROM {field_config} WHERE field_name = :name';
  $arguments = array(':name' => 'field_computed_ou');
  $data = unserialize(db_query($query, $arguments)
    ->fetchField());
  $data['settings']['max_length'] = (string) $length;
  $data['settings']['database']['data_length'] = $length;

  db_update('field_config')
    ->fields(array('data' => serialize($data)))
    ->condition('field_name', 'field_computed_ou')
    ->execute();

  $specification = array('type' => 'varchar', 'length' => $length);
  db_change_field('field_data_field_computed_ou', 'field_computed_ou_value', 'field_computed_ou_value', $specification);
  db_change_field('field_revision_field_computed_ou', 'field_computed_ou_value', 'field_computed_ou_value', $specification);

  field_info_cache_clear();
}

/**
 * Migrate the logo field from a file managed field to a Scald atom field.
 */
function paddle_organizational_unit_update_7002(&$sandbox) {
  // Enable the module defining the scald atom field.
  module_enable(array('paddle_scald_atom_field'));

  // Get all the nodes which have the field_paddle_ou_logo field.
  if (!isset($sandbox['nids'])) {
    $field = field_info_field('field_paddle_ou_logo');
    if (!empty($field['bundles']['node'])) {
      $bundles = $field['bundles']['node'];
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')->propertyCondition('type', $bundles);
      $results = $query->execute();

      if (!empty($results['node'])) {
        // Setup data for running the steps needed for the updates.
        $sandbox['#finished'] = 0;
        $sandbox['nids'] = array_keys($results['node']);
        $sandbox['field_values'] = array();
        $sandbox['total'] = count($results['node']);
        $sandbox['totalSave'] = 0;
        // Holds the number of processed nodes.
        $sandbox['count'] = 0;
        $sandbox['countSave'] = 0;
        $sandbox['rate'] = 5;
        // Go to the next step of the update.
        return;
      }
    }

    // Either no node bundles or nodes have been found. Just update the field.
    _paddle_organizational_unit_update_recreate_ou_logo_field();
    $sandbox['#finished'] = 1;
  }
  // Save the logo image data.
  elseif (count($sandbox['nids']) > $sandbox['count']) {
    // Take part of the nodes to process.
    $nids = array_slice($sandbox['nids'], $sandbox['count'], $sandbox['rate']);
    foreach ($nids as $nid) {
      $node = entity_load_single('node', $nid);
      $value = field_get_items('node', $node, 'field_paddle_ou_logo');
      if ($value) {
        $sandbox['field_values'][$nid] = $value[0];
      }
      $sandbox['#finished'] = abs((($sandbox['count'] - 1) / $sandbox['total']) / 2);
      $sandbox['count']++;
    }
  }
  elseif (is_array($sandbox['nids']) && count($sandbox['nids']) === $sandbox['count']) {
    $sandbox['nids'] = -1;
    $sandbox['totalSave'] = count($sandbox['field_values']);

    _paddle_organizational_unit_update_recreate_ou_logo_field();

    $sandbox['#finished'] = abs(((($sandbox['count']) - 1) / $sandbox['total']) / 2);
  }
  // Create an atom for this node and attach it the field.
  elseif (count($sandbox['field_values']) > $sandbox['countSave']) {
    $nids = array_slice($sandbox['field_values'], $sandbox['countSave'], $sandbox['rate'], TRUE);
    foreach ($nids as $nid => $file) {
      $node = entity_load_single('node', $nid);

      // Create an atom from the file.
      $options = array(
        'alt_text' => $file['filename'],
      );
      try {
        $atom_id = paddle_update_create_atom((object) $file, $options);
        $wrapper = entity_metadata_wrapper('node', $node);
        $wrapper->field_paddle_ou_logo->set(array('sid' => $atom_id));
        $wrapper->save();
      }
      catch (\Exception $e) {
        // Nothing we can do.
      }
      $sandbox['#finished'] = abs(0.5 + (((($sandbox['countSave']) - 1) / $sandbox['totalSave']) / 2));
      $sandbox['countSave']++;
    }
  }
  else {
    $sandbox['#finished'] = 1;
  }
}

/**
 * Helper function to remove the current ou logo field and update it.
 */
function _paddle_organizational_unit_update_recreate_ou_logo_field() {
  // Delete the OU logo image field.
  field_delete_field('field_paddle_ou_logo');
  field_info_cache_clear();

  // Create the new field by reverting the features.
  features_include_defaults(NULL, TRUE);
  $revert = array(
    'paddle_organizational_unit' => array('field_base', 'field_instance'),
  );
  features_revert($revert);

  field_info_cache_clear();
}

/**
 * Update existing panes used to render the OU logo to use the new approach.
 */
function paddle_organizational_unit_update_7003() {
  $limit = 5;

  // @codingStandardsIgnoreStart
  paddle_update_batch_helper(
    $sandbox,
    $limit,
    // Count callback.
    function (&$sandbox) {
      $q = db_select('panels_pane', 'p')
        ->condition('type', 'entity_field')
        ->condition('subtype', 'node:field_paddle_ou_logo')
        ->countQuery();
      return $q->execute()->fetchField();
    },
    // Range retrieval callback.
    function ($limit, &$sandbox) {
      $offset = $sandbox['progress'];

      $q = db_select('panels_pane', 'p')
        ->condition('type', 'entity_field')
        ->condition('subtype', 'node:field_paddle_ou_logo')
        ->fields('p', array());

      $q->range($offset, $limit);

      $results = $q->execute();

      return $results;
    },
    // Single item update callback.
    function ($db_item, &$sandbox) {
      // Ensure we unserialize all serialized fields with
      // ctools_export_unpack_object(). Fields that are not unserialized,
      // would otherwise become double-serialized when saving them again.
      // See panels_load_displays().
      ctools_include('export');
      $item = ctools_export_unpack_object('panels_pane', $db_item);

      // Change the configuration to reflect the new approach.
      $item->configuration = array(
        'label' => 'hidden',
        'formatter' => 'paddle_organizational_unit_logo',
        'delta_limit' => 0,
        'delta_offset' => '0',
        'delta_reversed' => FALSE,
        'formatter_settings' => array(),
        'context' => 'panelizer',
        'override_title' => 0,
        'override_title_text' => '',
        'override_title_heading' => 'h2',
      );
      drupal_write_record('panels_pane', $item, 'pid');

      return TRUE;
    },
    // Progress message callback.
    function ($sandbox) {
      $item = $sandbox['last_item'];
      $updated = $sandbox['last_item_update_status'];

      $id_string = "pane {$item->pid}";

      if ($updated) {
        return $id_string;
      }
      else {
        return "{$id_string} SKIPPED";
      }
    }
  );
  // @codingStandardsIgnoreEnd
}
