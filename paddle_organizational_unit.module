<?php

/**
 * @file
 * Code for the Paddle Organizational Unit app.
 */

include_once 'paddle_organizational_unit.features.inc';

/**
 * Implements hook_apps_app_info().
 */
function paddle_organizational_unit_apps_app_info() {
  return array();
}

/**
 * Implements hook_theme().
 */
function paddle_organizational_unit_theme($existing, $type, $theme, $path) {
  return array(
    'paddle_organizational_unit_short' => array(
      'path' => $path . '/templates',
      'template' => 'paddle-organizational-unit-short',
    ),
    'paddle_organizational_unit_medium' => array(
      'path' => $path . '/templates',
      'template' => 'paddle-organizational-unit-medium',
    ),
    'paddle_organizational_unit_long' => array(
      'path' => $path . '/templates',
      'template' => 'paddle-organizational-unit-long',
    ),
    'paddle_organizational_unit_page_contact_information' => array(
      'path' => $path . '/templates',
      'template' => 'paddle-organizational-unit-page-contact-information',
    ),
    'paddle_organizational_unit_page_info' => array(
      'path' => $path . '/templates',
      'template' => 'paddle-organizational-unit-page-info',
    ),
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function paddle_organizational_unit_ctools_plugin_directory($module, $plugin) {
  $types = array(
    'ctools:content_types',
  );
  if (in_array("$module:$plugin", $types)) {
    return "plugins/$plugin";
  }
}

/**
 * Implements hook_paddle_content_manager_additional_fields().
 */
function paddle_organizational_unit_paddle_content_manager_additional_fields() {
  return array(
    'field_paddle_ou_logo',
    'field_paddle_ou_parent_1',
    'field_paddle_ou_parent_2',
    'field_paddle_ou_head_unit',
    'field_paddle_ou_address',
    'field_paddle_ou_phone',
    'field_paddle_ou_fax',
    'field_paddle_ou_email',
    'field_paddle_ou_website',
  );
}

/**
 * Implements hook_paddle_content_manager_additional_fields_groups().
 */
function paddle_organizational_unit_paddle_content_manager_additional_fields_groups() {
  return array(
    'contact information' => array(
      'label' => t('Contact information'),
      'weight' => 1,
      'fields' => array(
        'field_paddle_ou_address',
        'field_paddle_ou_phone',
        'field_paddle_ou_fax',
        'field_paddle_ou_email',
        'field_paddle_ou_website',
      ),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function paddle_organizational_unit_menu() {
  $items = array();

  $items['admin/organizational_unit/node_autocomplete_callback'] = array(
    'page callback' => 'paddle_organizational_unit_node_autocomplete_callback',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );

  return $items;
}

/**
 * Autocomplete callback for nodes by title or real path.
 *
 * @param string $parent_lang
 *   The language of the parent node where the autocomplete is being called.
 * @param string $string
 *   The string that will be searched.
 * @param int $nid
 *   When the user searches for "node/<nid>" this will hold the nid.
 *
 * Searches for a node by title or real path, but then identifies it by nid,
 * so the actual returned value can be used later by the form.
 *
 * The returned $matches array has
 * - key: The title, with the identifying node path in brackets, like
 *     "Some node title (node/444)"
 * - value: the title which will be displayed in the autocompleted dropdown.
 */
function paddle_organizational_unit_node_autocomplete_callback($parent_lang = '', $string = '', $nid = 0) {
  global $language_content;

  $language = !empty($parent_lang) ? $parent_lang : $language_content->language;
  $results = array();
  if ($string) {
    $query = db_select('node', 'n');
    $query->join('field_data_field_computed_ou', 'cou', 'n.nid = cou.entity_id');
    $query->join('workbench_moderation_node_history', 'wmh', 'wmh.nid = n.nid AND wmh.vid = n.vid');
    $query->groupBy('cou.entity_id');
    $query->fields('n', array('nid', 'title'))
      ->fields('cou', array('field_computed_ou_value'))
      ->condition('wmh.state', 'archived', '<>')
      ->condition('wmh.is_current', 1, '=')
      ->condition('n.language', $language, '=')
      ->condition('n.type', 'organizational_unit', '=')
      ->addTag('node_access');

    if ($string == 'node' && is_numeric($nid) && $nid !== 0) {
      // The search string is a node path.
      $query->condition('n.nid', $nid, '=');
    }
    elseif ($string != 'node') {
      // The search string is a title or something else.
      $args = func_get_args();
      // Remove the first param which should be the language.
      unset($args[0]);
      $like = db_like(implode('/', $args));
      // If the url ends on "/" we assume that this "/" is part of the title
      // we are looking for (of course the $nid should be 0).
      if (substr(request_uri(), -1) == '/') {
        $like .= '/';
      }
      $query->condition('cou.field_computed_ou_value', '%' . $like . '%', 'LIKE');
    }

    // If neither is true we basically show the first 10 nodes we can find.
    $query->range(0, 10);
    $result = $query->execute();
    foreach ($result as $node) {
      $results[$node->field_computed_ou_value . " (node/$node->nid)"] = check_plain($node->field_computed_ou_value) . " (node/$node->nid)";
    }
  }

  drupal_json_output($results);
}

/**
 * Set the value of the computed field of the organizational unit.
 *
 * We combine title, parent 1 and parent 2.
 */
function computed_field_field_computed_ou_compute(&$entity_field, $entity_type, $entity, $field, $instance, $langcode, $items) {
  $entity_field[0]['value'] = '';

  $parent1 = field_get_items($entity_type, $entity, 'field_paddle_ou_parent_1');
  if (!empty($parent1[0]['value'])) {
    $entity_field[0]['value'] .= $parent1[0]['value'] . ' > ';
  }

  $parent2 = field_get_items($entity_type, $entity, 'field_paddle_ou_parent_2');
  if (!empty($parent2[0]['value'])) {
    $entity_field[0]['value'] .= $parent2[0]['value'] . ' > ';
  }

  $entity_field[0]['value'] .= $entity->title;
}

/**
 * Implements hook_field_widget_form_alter().
 */
function paddle_organizational_unit_field_widget_form_alter(&$element, &$form_state, $context) {
  if ($context['field']['field_name'] == 'field_paddle_ou_address') {
    // Don't display address input fields in a separate fieldset.
    unset($element['#type']);
  }
  if ($context['field']['field_name'] == 'field_paddle_ou_website') {
    // Don't display url input field in a fieldset.
    unset($element['#type']);
  }
}

/**
 * Implements hook_paddle_content_region_content_type_info().
 */
function paddle_organizational_unit_paddle_content_region_content_type_info() {
  return 'organizational_unit';
}

/**
 * Implements hook_field_formatter_info().
 */
function paddle_organizational_unit_field_formatter_info() {
  return array(
    'paddle_organizational_unit_logo' => array(
      'label' => t('Organizational unit logo'),
      'field types' => array('paddle_scald_atom'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function paddle_organizational_unit_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  foreach ($items as $index => $item) {
    $atom = scald_atom_load($item['sid']);
    $attributes = array('class' => array('paddle-organizational-unit-logo'));

    if ($display['type'] == 'paddle_organizational_unit_logo') {
      // Determine the image style, if any.
      $image_style = !empty($item['style']) ? $item['style'] : 'medium';
      // Render the image markup.
      $element[$index]['#markup'] = theme('paddle_scald_render_atom', array(
        'atom' => $atom,
        'image_style' => $image_style,
        'attributes' => $attributes,
      ));
    }
  }
  return $element;
}
